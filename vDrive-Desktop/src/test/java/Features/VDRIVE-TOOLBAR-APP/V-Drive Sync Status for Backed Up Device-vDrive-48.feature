#@Regression
@Scenario48
Feature: V-Drive Sync Status for Backed Up Device-vDrive-48


Scenario Outline: Verify V-Drive Sync Status for Backed Up
    Given Open Application and User is on Login screen for Sync Backed Up
    When User enters "<Username>" and "<Password>" and "<Secretans>" for Sync Backed Up
  	And User click on Add Device and continue for Sync Backed Up
    And Dashboard will display and Verify Sync and Update Status
    Then Close vDriver Application vDrive48
    Examples:
      |Username|Password|Secretans|
      |9085668110|sncrqa123|pizza|