#@Regression
@Scenario13
Feature: New Device Application Login-vDrive13 



Scenario: Take user to first onboarding screen/continue with VZ 

	Given Open vDriver Desktop Application For Onboarding screen 
	Then Verifying Onboarding screen and continue Button for vDriver Desktop Application 
	Then Close vDriver Application vDrive13 
	
	
	

Scenario Outline: First time user login flow upto V-Drive Start-Up screen 

	Given Open vDriver Desktop Application For First time user 
	When Click on Continue Button and Open Login Screen 
	And User enters "<Username>" and "<Password>" and "<Secretans>" for Login 
	And Verify vDrive Startup screen 
	Then Close vDriver Application First vDrive13 
	
	
	Examples: 
		|Username|Password|Secretans|
		|9085668110|sncrqa123|pizza|  
		
		

Scenario: Take user to the VZ login window for 2nd or higher time login 
		
	Given Open vDriver Desktop Application For login window for 2nd time 
	When Verify Onboarding screen did not displayed but Only Login Page will Display
	Then Close vDriver Application vDrive13 
			
			
			
