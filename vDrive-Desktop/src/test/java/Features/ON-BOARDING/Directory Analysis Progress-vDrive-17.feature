#@Regression
@Scenario17
Feature: Directory Analysis Progress-vDrive-17

Scenario Outline: Automatically Directory Analysis Progress after Clicking On 'Add Device' from the start-up screen.


	Given Open vDriver Desktop Application Directory analysis Progress
    When User enters valid "<Username>" and "<Password>" and "<Secretans>" for LogIn 
  	And User click on Add Device and Verify the start-up and Analysis Complete Progress
    Then Close vDriver Application vDrive17
    Examples:
      |Username|Password|Secretans|
      |9085668110|sncrqa123|pizza|