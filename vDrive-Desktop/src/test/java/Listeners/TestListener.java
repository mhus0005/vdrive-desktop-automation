package Listeners;




import com.aventstack.extentreports.ExtentReports;


//import com.aventstack.extentreports.gherkin.model.Feature;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;


public class TestListener extends ExtentReportListener  implements ITestListener {    

   private static ExtentReports extent;
   
 



    public void onTestStart(ITestResult result) {
    	
    }

    public void onTestSuccess(ITestResult result) {

    }

    public void onTestFailure(ITestResult result) {
    	
   
    }

    public void onTestSkipped(ITestResult result) {

    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    public void onStart(ITestContext context) {

        extent=setUp();
  

    }

    public void onFinish(ITestContext context) {
    	extent.flush();
        System.out.println("Generated The Test Report");
    }
}
