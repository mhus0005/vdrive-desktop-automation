package Listeners;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
//import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.markuputils.ExtentColor;
//import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import managers.Driver;
import org.sikuli.script.App;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.sikuli.script.ScreenImage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
import java.io.IOException;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
//import java.util.Base64;
import java.util.Date;

public class ExtentReportListener extends Base{
	
	
	public static ExtentHtmlReporter report = null;
    public static ExtentTest test = null;
    public static ExtentReports extent = null;
	



    public static ExtentReports setUp(){
        String reportLocation = "./Reports/vDrive_"+getCurrentDateTime()+".html";
        report = new ExtentHtmlReporter(reportLocation);
        report.config().setDocumentTitle("vDrive-Desktop Automation Test Report");
        report.config().setReportName("vDrive-Desktop Automation Test Report");
        report.config().setTheme(Theme.DARK);
        report.config().setEncoding("utf-8");
        System.out.println("Extent Report Location initialized.....");
        report.start();

        extent = new ExtentReports();
        extent.attachReporter(report);
        extent.setSystemInfo("User Name","MOHAMMAD RAJIB HUSSAIN");
        extent.setSystemInfo("Application Name","vDriver Dektop Application");
        extent.setSystemInfo("Operating System", System.getProperty("os.name"));
        extent.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
        System.out.println("System Info. set in Extent Report");
     return extent;
    }   


    public static void testStepHandler(String teststatus,ExtentTest extentTest,Throwable throwable){
        switch (teststatus) {
            case "FAIL":
                extentTest.fail(MarkupHelper.createLabel("Test Case is Failed : ", ExtentColor.RED));
                extentTest.error(throwable.fillInStackTrace());
                try{
                    extentTest.addScreencastFromPath(captureScreenshot());
                } catch (IOException e){
                    e.printStackTrace();
                }
                break;

            case "PASS":
                extentTest.pass(MarkupHelper.createLabel("Test Case is Passed : ", ExtentColor.GREEN));
                break;

            default:
                break;
        }

    } 

    public static Screen getScreenDriver() {
        return Driver.getInstance();
    }

    public static String captureScreenshot() throws IOException {

        String imagePath = "";
        	Region focusWindow = App.focusedWindow();
            ScreenImage WindowRegion = getScreenDriver().capture(focusWindow);
            BufferedImage src = WindowRegion.getImage();
            imagePath = "./ScreenShots/screenshot.png";
            File file = new File(imagePath);
            ImageIO.write(src, "png", file);

        return imagePath;
    }


    public static String getCurrentDateTime() {
        DateFormat customFormat= new SimpleDateFormat("MM_dd_yyyy_HH_mm_ss");
        Date customDate=new Date();
        return customFormat.format(customDate);
    }

 

}
