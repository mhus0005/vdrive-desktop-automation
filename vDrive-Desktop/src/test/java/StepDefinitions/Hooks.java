package StepDefinitions;


//import Listeners.Base;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import helper.loadPropeties;
import screens.AbstractScreen;
import screens.LogInScreen;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sikuli.script.App;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import java.io.IOException;


public class Hooks {
	
	
	public static Screen screenDriver;
    public static Region windowDriver;
	public static Logger log;
    public loadPropeties config;
    public static ExtentHtmlReporter report = null;
    public static ExtentTest test = null;
    public static ExtentReports extent = null;


   @Before
    public void setUpSuite() throws IOException{
        log = LogManager.getLogger(Hooks.class.getName());
        config =new loadPropeties();
    }
   
   
    @Before
    public void CheckApp() {
    	
    	LogOut logOff = new LogOut();
    	try {
			logOff.closeExistingApp();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
    public void loginToTheApp() throws InterruptedException {
        AbstractScreen ab = new AbstractScreen();
   //     log.info("Before starting the execution, check for Login Screen and login!!!!");
            LogInScreen login = new LogInScreen();	                
                try {
                	 if (login.isvDriveOnboardingPageExist()) {
 	            //    	log.info("The Verizon Drive Cloud App Exist");
 	                } else if(login.isLoginWindowExist()) {
 	             //   	log.info("User already Login Screen");
 	                } else {
 	                    

 	                   try {
 	                	//  App.open(config.getAppLocation());
 	                      App.open("C://Users//mhus0005//AppData//Local//VerizonDrive//Verizon Drive.exe");
 	                     ab.Sleep(10000);
 	             //        log.info("Opening vDriver Application");
 	                      }
 	                   	  
 		                catch (Exception e) {
 		                    e.printStackTrace();
 		                    log.info("Fail to OpenvDrive Application");
 		                    Assert.fail();
 		                }  
 	                }
                }
                catch (Exception e) {
                    e.printStackTrace();
              //      log.info("Exception found");
                    Assert.fail();
                }  


    }




    public void logOff() throws InterruptedException, IOException {
        LogOut logOff = new LogOut();
        AbstractScreen ab = new AbstractScreen();
        ab.Sleep(5000);
        logOff.clickLogOff();
    }

    @After
    public void tearDownTest(Scenario scenario) throws IOException {
        String TestName= scenario.getName();
        System.out.println(TestName);
        if(scenario.isFailed()){
        	System.out.println("it Failed");
        } else {
        	System.out.println("it Passed");
        }

    }
    
    
    
    




}
