package StepDefinitions;

import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;

import Listeners.ExtentReportListener;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import screens.AbstractScreen;
import screens.LogInScreen;

public class SkipBackup20 extends ExtentReportListener{
		
		Hooks hk = new Hooks();
	
	 	@Given("^Open vDriver Desktop Application For Skip Backup$")
	    public void open_vdriver_desktop_application_for_skip_backup() throws Throwable {
			
			ExtentTest logInfo=null;
	    	
	    	try {
	    	test = extent.createTest(Feature.class, "LogIn and Skip Backup-vDrive-20");
	        test=test.createNode(Scenario.class, "LogIn and Skip Backup");
	        logInfo=test.createNode(new GherkinKeyword("Given"),"Open_vdriver_desktop_application_for_skip_backup");
	        
	        AbstractScreen ab = new AbstractScreen();
	        LogInScreen login = new LogInScreen();

	        hk.loginToTheApp();
	        ab.Sleep(15000);
	        if (login.isLoginWindowExist()){
	  
	        	logInfo.pass("User already in LogIn screen");
	 
	        } else if (login.isresetLoginWindowExist()) {
	        	logInfo.pass("User already in LogIn screen");
	        }
	        else if(login.VdriveContinueButtonExist()) {
	        	
	            login.clickVdriveContinueButton();
	            logInfo.info("It clikc on Continue Verizon Button");
	            ab.Sleep(10000);
	            if (login.isLoginWindowExist()) {
	            logInfo.pass("User is in LogIn screen");

	            } else {
	            logInfo.fail("Login screen does not exist");;

	            } 
	        }
	    	} catch (AssertionError | Exception e) {
	    		testStepHandler("FAIL",logInfo,e);
	    	}
	    	
	    }

	    @When("^User enters \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" for Skip Backup$")
	    public void user_enters_something_and_something_and_something_for_skip_backup(String strArg1, String strArg2, String strArg3) throws Throwable {

	    	ExtentTest logInfo=null;
	    	 
	   	 try {
	   		 logInfo= test.createNode(new GherkinKeyword("When"),"User_enters_Username_and_Password_and_SecretAns_for_skip_backup");
	       AbstractScreen ab = new AbstractScreen();
	       LogInScreen login = new LogInScreen();
	       ab.Sleep(8000);
	       System.out.println("User ID and Password will be provided");
	       if (login.isLoginWindowExist()) {
	           login.enterLoginData(strArg1, strArg2);
	           logInfo.info("User Credentials entered");
	           login.clickLogIn();
	           ab.Sleep(15000);
	                   if(login.homePageExist()) {
	                   	logInfo.pass("LogIn Successful");
	                   } else if(login.secterQusPageExist()) {
	                       login.provideSecretQus(strArg3);
	                       logInfo.pass("Looking for the Secret Ans");
	                       login.clickSecretContinueButton();
	                       ab.Sleep(15000);

	                       
	                   } else if(login.verificationPageExist()) {
	                   	logInfo.info("Need Verification To Login");
	                       login.verificationSendButton();
	                       ab.Sleep(15000);
	                       if (login.isLoginWindowExist()) {
	                           login.enterLoginData(strArg1, strArg2);
	                           logInfo.pass("User Credentials entered");
	                     //      logInfo.pass("User Logged In");
	                           login.clickLogIn(); }
	                   }
	                   else {
	                   	logInfo.fail("User having issue with LogIn");

	                       Assert.fail();
	                   }

	       } else if (login.isresetLoginWindowExist()){
	       	login.enterresetLoginData(strArg1, strArg2);
	           logInfo.info("User Credentials entered");
	           login.clickresetLogIn();
	           ab.Sleep(15000);
	       	if(login.homePageExist()) {
	           	logInfo.pass("LogIn Successful");
	           } else if(login.secterQusPageExist()) {
	               login.provideSecretQus(strArg3);
	               logInfo.pass("Looking for the Secret Ans");
	               login.clickSecretContinueButton();
	               ab.Sleep(15000);

	               
	           } else if(login.verificationPageExist()) {
	           	logInfo.info("Need Verification To Login");
	               login.verificationSendButton();
	               ab.Sleep(15000);
	               if (login.isLoginWindowExist()) {
	                   login.enterLoginData(strArg1, strArg2);
	                   logInfo.pass("User Credentials entered");
	             //      logInfo.pass("User Logged In");
	                   login.clickLogIn(); }
	           }
	           else {
	           	logInfo.fail("User having issue with LogIn");

	               Assert.fail();
	           }
	       }
	       	else {
	      
	       	logInfo.fail("LogIn Page did not Exist Test Case Failed");

	           Assert.fail();
	       }

	   	 } catch (AssertionError | Exception e) {
	    		testStepHandler("FAIL",logInfo,e);
	    		Assert.fail();
	    	}
	       
	    }

	    @Then("^Click on 'Continue without adding' and Skip Backup$")
	    public void click_on_continue_without_adding_and_skip_backup() throws Throwable {
	    	
	    	 
	    	ExtentTest logInfo=null;
			 try {
				 logInfo= test.createNode(new GherkinKeyword("And"),"Click_on_continue_without_adding_and_skip_backup");
	       AbstractScreen ab = new AbstractScreen();
	       LogOut logOff = new LogOut();
	       LogInScreen login = new LogInScreen();
	       ab.Sleep(15000);
	       if (login.homePageExist()) {
	       	logInfo.pass("Home Page Exist");

	           login.ClickContinuewithOutAddingButton();
	           ab.Sleep(15000);
	           if (login.backupProgressPageExist()) {
	           		logInfo.pass("BackupProgress Page Exist");
	      
	               login.clickbackUpContinueButton();
	               ab.Sleep(5000);
	               logOff.clickOpeniconButton();
	              if( logOff.greenSyncExist()) {
	            	  logInfo.pass("Setup complete screen without backup was successfull");
	            	  logOff.clickOpeniconButton();
	              } else {
	               logInfo.fail("Setup complete screen without backup was Not successfull");
	               logOff.clickOpeniconButton();
	              }
	           } else {
	           	logInfo.fail("BackupProgress Page does not exists");
	           }
	       } else {
	       	logInfo.fail("Home Page does not exists");

	       }
			 } catch (AssertionError | Exception e) {
		     		testStepHandler("FAIL",logInfo,e);
		     	}
	        
	    }

	    @And("^Close vDriver Application vDrive20$")
	    public void close_vdriver_application_vdrive20() throws Throwable {

	    	ExtentTest logInfo=null;
	    	LogOut logOff = new LogOut();
	    	try {
				 logInfo= test.createNode(new GherkinKeyword("Then"),"Close_vdriver_application");
	    	hk.logOff();
	    	logOff.ResetWithQuitApp();
	    	} catch (AssertionError | Exception e) {
	    		testStepHandler("FAIL",logInfo,e);
	    	}
	       
	    }
}
