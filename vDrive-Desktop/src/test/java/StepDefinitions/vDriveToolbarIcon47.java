package StepDefinitions;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;

import Listeners.ExtentReportListener;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import screens.AbstractScreen;
import screens.LogInScreen;

public class vDriveToolbarIcon47 extends ExtentReportListener{
	
	Hooks hk = new Hooks();
	
	@Given("^Open vDriver Desktop Application For V-Drive Toolbar Icon$")
    public void open_vdriver_desktop_application_for_vdrive_toolbar_icon() throws Throwable {

		ExtentTest logInfo=null;
    	
    	try {
    	test = extent.createTest(Feature.class, "V-Drive Toolbar Icon-vDrive-47");
        test=test.createNode(Scenario.class, "V-Drive Toolbar Icon Verification.");
        logInfo=test.createNode(new GherkinKeyword("Given"),"Open_vdriver_desktop_application_for_vdrive_toolbar_icon");
        
        AbstractScreen ab = new AbstractScreen();
        LogInScreen login = new LogInScreen();

        hk.loginToTheApp();
        ab.Sleep(15000);
        if (login.isLoginWindowExist()){
  
        	logInfo.pass("User already in LogIn screen");
 
        } else if (login.isresetLoginWindowExist()) {
        	logInfo.pass("User already in LogIn screen");
        }
        else if(login.VdriveContinueButtonExist()) {
        	
            login.clickVdriveContinueButton();
            logInfo.info("It clikc on Continue Verizon Button");
            ab.Sleep(10000);
            if (login.isLoginWindowExist()) {
            logInfo.pass("User is in LogIn screen");

            } else {
            logInfo.fail("Login screen does not exist");;

            } 
        }
    	} catch (AssertionError | Exception e) {
    		testStepHandler("FAIL",logInfo,e);
    	}
    	
        
    }

    @Then("^Verify V-Drive Cloud icon and toolbar icon functions$")
    public void verify_vdrive_cloud_icon_and_toolbar_icon_functions() throws Throwable {
    	

    	ExtentTest logInfo=null;
		 try {
			 logInfo= test.createNode(new GherkinKeyword("Then"),"Verify_vdrive_cloud_icon_and_toolbar_icon_functions");
       AbstractScreen ab = new AbstractScreen();
       LogOut logOff = new LogOut();
       LogInScreen login = new LogInScreen();
       ab.Sleep(8000);
       logOff.clickOpeniconButton();
       if (logOff.vDriveIconExist()) {
       	logInfo.pass("vDrive Icon Exist");
       	ab.Sleep(3000);
       	logOff.OpenvDriveIcon();
       	if (login.isLoginWindowExist()) {
       		logInfo.pass("Onboarding current screen Displayed");
       	} else {
       		logInfo.fail("Onboarding current screen Did Not Displayed");
       	}
       	logOff.ClickOnDotsManu();
       	if (logOff.SettingButtonExist()) {
       		logInfo.pass("SettingButton Exist in ToolBar Manu");
       	} else {
       		logInfo.fail("SettingButton Does Not Exist in ToolBar Manu");
       	}
       	
       	if (logOff.QuitButtonExist()) {
       		
       		logInfo.pass("QuitButton Exist in ToolBar Manu");
       	} else {
       		logInfo.fail("QuitButton Does Not Exist in ToolBar Manu");
       	}
       	
       	if (logOff.LogOutButtonExist()) {
       		
       		logInfo.pass("LogOutButton Exist in ToolBar Manu");
       	} else {
       		logInfo.fail("LogOutButton Does Not Exist in ToolBar Manu");
       	}
       		
       	
       	
       } else {
       	logInfo.fail("vDrive Icon does not exists");

       }
		 } catch (AssertionError | Exception e) {
	     		testStepHandler("FAIL",logInfo,e);
	     	}
        
       
        
    }

    @And("^Close vDriver Application vDrive47$")
    public void close_vdriver_application_vdrive47() throws Throwable {
        

    	ExtentTest logInfo=null;
    	LogOut logOff = new LogOut();
    	try {
			 logInfo= test.createNode(new GherkinKeyword("Then"),"Close_vdriver_application");
    	hk.logOff();
    	logOff.ResetWithQuitApp();
    	} catch (AssertionError | Exception e) {
    		testStepHandler("FAIL",logInfo,e);
    	}
    }


}
