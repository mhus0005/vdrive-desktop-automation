package StepDefinitions;

import org.junit.runner.RunWith;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;

import Listeners.ExtentReportListener;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.junit.Cucumber;
import screens.AbstractScreen;
import screens.LogInScreen;

@RunWith(Cucumber.class)
public class VDriveStartupScreen18 extends ExtentReportListener{
	
	Hooks hk = new Hooks();
	
	@Given("^Open vDriver Desktop Application For Start-up Screen$")
    public void open_vdriver_desktop_application_for_startup_screen() throws Throwable {
ExtentTest logInfo=null;
    	
    	try {
    	test = extent.createTest(Feature.class, "V-Drive Start-up Screen-vDrive18");
        test=test.createNode(Scenario.class, "Application automatically initiates directory analysis after the user selects 'Add Device' from the start-up screen");
        logInfo=test.createNode(new GherkinKeyword("Given"),"open_vdriver_desktop_application_directory_analysis");
        
        AbstractScreen ab = new AbstractScreen();
        LogInScreen login = new LogInScreen();

        hk.loginToTheApp();
        ab.Sleep(15000);
        if (login.isLoginWindowExist()){
  
        	logInfo.pass("User already in LogIn screen");
 
        } else if (login.isresetLoginWindowExist()) {
        	logInfo.pass("User already in LogIn screen");
        }
        else if(login.VdriveContinueButtonExist()) {
        	
            login.clickVdriveContinueButton();
            logInfo.info("It click on Continue Verizon Button");
            ab.Sleep(10000);
            if (login.isLoginWindowExist()) {
            logInfo.pass("User is in LogIn screen");
            } else if (login.isresetLoginWindowExist()){
            	logInfo.pass("User is in LogIn screen");
            } else {
            logInfo.fail("Login screen does not exist");
            

            } 
        }
    	} catch (AssertionError | Exception e) {
    		testStepHandler("FAIL",logInfo,e);
    	}
    	
    	
        
       
    }

    @When("^User enters \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" for login$")
    public void user_enters_something_and_something_and_something_for_login(String strArg1, String strArg2, String strArg3) throws Throwable {

    	ExtentTest logInfo=null;
    	 
   	 try {
   		 logInfo= test.createNode(new GherkinKeyword("When"),"User_enters_Username_and_Password_and_SecretAns__for_login");
       AbstractScreen ab = new AbstractScreen();
       LogInScreen login = new LogInScreen();
       ab.Sleep(8000);
       System.out.println("User ID and Password will be provided");
       if (login.isLoginWindowExist()) {
           login.enterLoginData(strArg1, strArg2);
           logInfo.info("User Credentials entered");
           login.clickLogIn();
           ab.Sleep(15000);
                   if(login.homePageExist()) {
                   	logInfo.pass("LogIn Successful");
                   } else if(login.secterQusPageExist()) {
                       login.provideSecretQus(strArg3);
                       logInfo.pass("Looking for the Secret Ans");
                       login.clickSecretContinueButton();
                       ab.Sleep(15000);

                       
                   } else if(login.verificationPageExist()) {
                   	logInfo.info("Need Verification To Login");
                       login.verificationSendButton();
                       ab.Sleep(15000);
                       if (login.isLoginWindowExist()) {
                           login.enterLoginData(strArg1, strArg2);
                           logInfo.pass("User Credentials entered");
                           login.clickLogIn(); }
                   }
                   else {
                   	logInfo.fail("User having issue with LogIn");
                   	Assert.fail("User having issue with LogIn");	
                   }

       } else if (login.isresetLoginWindowExist()){
       	login.enterresetLoginData(strArg1, strArg2);
           logInfo.info("Reset User Credentials entered");
           login.clickresetLogIn();
           ab.Sleep(20000);
       	if(login.homePageExist()) {
           	logInfo.pass("LogIn Successful");
           } 
       	else if(login.secterQusPageExist()) {
               login.provideSecretQus(strArg3);
               logInfo.pass("Looking for the Secret Ans");
               login.clickSecretContinueButton();
               ab.Sleep(15000);               
           } 
       	else if(login.verificationPageExist()) {
           	logInfo.info("Need Verification To Login");
               login.verificationSendButton();
               ab.Sleep(15000);
               if (login.isLoginWindowExist()) {
                   login.enterLoginData(strArg1, strArg2);
                   logInfo.pass("User Credentials entered");
                   login.clickLogIn(); }
           }
           else {
           	logInfo.fail("User having issue with LogIn");
           	Assert.fail("User having issue with LogIn");
           }
       }
       	else {
      
       	logInfo.fail("LogIn Page did not Exist Test Case Failed");
       	Assert.fail("LogIn Page did not Exist Test Case Failed");
       }

   	 } catch (AssertionError | Exception e) {
    		testStepHandler("FAIL",logInfo,e);
    	}
       
    }

    @Then("^User will Verify vDrive Start-up Screen Functionalities$")
    public void user_will_verify_vdrive_startup_screen_functionalities() throws Throwable {

 
    	ExtentTest logInfo=null;
		 try {
		logInfo= test.createNode(new GherkinKeyword("Then"),"user_will_verify_vdrive_startup_screen_functionalities");
       AbstractScreen ab = new AbstractScreen();
       LogInScreen login = new LogInScreen();
       ab.Sleep(15000);
       if (login.homePageExist()) {
       	logInfo.pass("Start-up Screen Exist");
       		
       		if(login.StartupCloudExist()) {
       			logInfo.pass("Cloud Sign Exist in StartUp Page");
       		} else {
       			logInfo.fail("Cloud Sign Does Not Exist in StartUp Page");
       		}
       		
       		if (login.StartupArrowIconExist()) {
       			logInfo.pass("ArrowIcon Sign Exist in StartUp Page");
       		} else {
       			logInfo.fail("ArrowIcon Sign Does Not Exist in StartUp Page");
       		}
       		
       		if (login.AddDeviceButtonExist()) {
       			logInfo.pass("AddDevice Button Exist in StartUp Page");
       		} else {
       			logInfo.fail("AddDevice Button Does Not Exist in StartUp Page");
       		}
       		
       		if (login.StartupContinueButtonExist()) {
       			logInfo.pass("Continue Button Exist in StartUp Page");
       		} else {
       			logInfo.fail("Continue Button Does Not Exist in StartUp Page");
       		}

           login.clickAddDeviceButton();
           
          if (login.AnalyzingPageExist()) {
        	  logInfo.pass("Analyzing Page Exist");
        	  ab.Sleep(15000);
              if (login.backupProgressPageExist()) {
              	logInfo.pass("BackupProgress Page Exist");
                ab.Sleep(20000);
              } else {
                 	logInfo.fail("BackUp Page Does Not Exist and Device was Not Added");
              }
          } else {
        	  logInfo.pass("Analyzing Page Does Not Exist");
        	  Assert.fail();
          }
           
       } else {
       	logInfo.fail("Home Page does not exists");

       }
		 } catch (AssertionError | Exception e) {
	     		testStepHandler("FAIL",logInfo,e);
	     	}
    
       
    }

    @And("^Close vDriver Application vDrive18$")
    public void close_vdriver_application_vdrive18() throws Throwable {
    	ExtentTest logInfo=null;
    	LogOut logOff = new LogOut();
    	try {
			 logInfo= test.createNode(new GherkinKeyword("And"),"Close_vDriver_Application");
    	hk.logOff();
    	logOff.ResetWithQuitApp();
    	} catch (AssertionError | Exception e) {
    		testStepHandler("FAIL",logInfo,e);
    	}
    }

}
