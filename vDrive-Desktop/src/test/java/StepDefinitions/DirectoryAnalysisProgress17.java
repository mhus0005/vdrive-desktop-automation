package StepDefinitions;

import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;

import Listeners.ExtentReportListener;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import screens.AbstractScreen;
import screens.LogInScreen;

public class DirectoryAnalysisProgress17 extends ExtentReportListener{
	
	Hooks hk = new Hooks();

	 @Given("^Open vDriver Desktop Application Directory analysis Progress$")
	    public void open_vdriver_desktop_application_directory_analysis_progress() throws Throwable {
		 ExtentTest logInfo=null;
	    	
	    	try {
	    	test = extent.createTest(Feature.class, "Directory Analysis Progress-vDrive17");
	        test=test.createNode(Scenario.class, "Automatically Directory Analysis Progress after Clicking On 'Add Device' from the start-up screen.");
	        logInfo=test.createNode(new GherkinKeyword("Given"),"open_vdriver_desktop_application_directory_analysis_progress");
	        
	        AbstractScreen ab = new AbstractScreen();
	        LogInScreen login = new LogInScreen();

	        hk.loginToTheApp();
	        ab.Sleep(15000);
	        if (login.isLoginWindowExist()){
	  
	        	logInfo.pass("User already in LogIn screen");
	 
	        } else if (login.isresetLoginWindowExist()) {
	        	logInfo.pass("User already in LogIn screen");
	        }
	        else if(login.VdriveContinueButtonExist()) {
	        	
	            login.clickVdriveContinueButton();
	            logInfo.info("It click on Continue Verizon Button");
	            ab.Sleep(10000);
	            if (login.isLoginWindowExist()) {
	            logInfo.pass("User is in LogIn screen");
	            } else if (login.isresetLoginWindowExist()){
	            	logInfo.pass("User is in LogIn screen");
	            } else {
	            logInfo.fail("Login screen does not exist");
	            

	            } 
	        }
	    	} catch (AssertionError | Exception e) {
	    		testStepHandler("FAIL",logInfo,e);
	    	}
	    	
	    	
	        
	
	    }
	
	    @When("^User enters valid \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" for LogIn$")
	    public void user_enters_valid_something_and_something_and_something_for_login(String strArg1, String strArg2, String strArg3) throws Throwable {
	    	ExtentTest logInfo=null;
	    	 
	      	 try {
	      		 logInfo= test.createNode(new GherkinKeyword("When"),"user_enters_valid_Username_and_Password_and_SecretAns_for_login");
	          AbstractScreen ab = new AbstractScreen();
	          LogInScreen login = new LogInScreen();
	          ab.Sleep(8000);
	          System.out.println("User ID and Password will be provided");
	          if (login.isLoginWindowExist()) {
	              login.enterLoginData(strArg1, strArg2);
	              logInfo.info("User Credentials entered");
	              login.clickLogIn();
	              ab.Sleep(15000);
	                      if(login.homePageExist()) {
	                      	logInfo.pass("LogIn Successful");
	                      } else if(login.secterQusPageExist()) {
	                          login.provideSecretQus(strArg3);
	                          logInfo.pass("Looking for the Secret Ans");
	                          login.clickSecretContinueButton();
	                          ab.Sleep(15000);

	                          
	                      } else if(login.verificationPageExist()) {
	                      	logInfo.info("Need Verification To Login");
	                          login.verificationSendButton();
	                          ab.Sleep(15000);
	                          if (login.isLoginWindowExist()) {
	                              login.enterLoginData(strArg1, strArg2);
	                              logInfo.pass("User Credentials entered");
	                              login.clickLogIn(); }
	                      }
	                      else {
	                      	logInfo.fail("User having issue with LogIn");
	                      	Assert.fail("User having issue with LogIn");	
	                      }

	          } else if (login.isresetLoginWindowExist()){
	          	login.enterresetLoginData(strArg1, strArg2);
	              logInfo.info("Reset User Credentials entered");
	              login.clickresetLogIn();
	              ab.Sleep(20000);
	          	if(login.homePageExist()) {
	              	logInfo.pass("LogIn Successful");
	              } 
	          	else if(login.secterQusPageExist()) {
	                  login.provideSecretQus(strArg3);
	                  logInfo.pass("Looking for the Secret Ans");
	                  login.clickSecretContinueButton();
	                  ab.Sleep(15000);               
	              } 
	          	else if(login.verificationPageExist()) {
	              	logInfo.info("Need Verification To Login");
	                  login.verificationSendButton();
	                  ab.Sleep(15000);
	                  if (login.isLoginWindowExist()) {
	                      login.enterLoginData(strArg1, strArg2);
	                      logInfo.pass("User Credentials entered");
	                      login.clickLogIn(); }
	              }
	              else {
	              	logInfo.fail("User having issue with LogIn");
	              	Assert.fail("User having issue with LogIn");
	              }
	          }
	          	else {
	         
	          	logInfo.fail("LogIn Page did not Exist Test Case Failed");
	          	Assert.fail("LogIn Page did not Exist Test Case Failed");
	          }

	      	 } catch (AssertionError | Exception e) {
	       		testStepHandler("FAIL",logInfo,e);
	       	}
	    }
	
	    @Then("^Close vDriver Application vDrive17$")
	    public void close_vdriver_application_vdrive17() throws Throwable {
	    	ExtentTest logInfo=null;
	    	LogOut logOff = new LogOut();
	    	try {
				 logInfo= test.createNode(new GherkinKeyword("Then"),"Close_vDriver_Application");
	    	hk.logOff();
	    	logOff.ResetWithQuitApp();
	    	} catch (AssertionError | Exception e) {
	    		testStepHandler("FAIL",logInfo,e);
	    	}

	    }
	
	    @And("^User click on Add Device and Verify the start-up and Analysis Complete Progress$")
	    public void user_click_on_add_device_and_verify_the_startup_and_analysis_complete_progress() throws Throwable {
	    	

	    	ExtentTest logInfo=null;
			 try {
			logInfo= test.createNode(new GherkinKeyword("And"),"user_click_on_add_device_and_verify_the_startup_and_analysis_complete_progress");
	       AbstractScreen ab = new AbstractScreen();
	       LogInScreen login = new LogInScreen();
	       ab.Sleep(15000);
	       if (login.homePageExist()) {
	       	logInfo.pass("Home Page Exist");

	           login.clickAddDeviceButton();
	           
	          if (login.AnalyzingPageExist()) {
	        	  if(login.spinnerLogoExist()) {
	        		  logInfo.pass("Analyzing Page Displayed");
	        		  logInfo.pass("Spinner Logo Displayed");
	        	  } else {
	        		  logInfo.fail("Spinner Logo Did Not Displayed");
	        	  }
	        	  logInfo.pass("Analyzing Page Displayed");
	        	  ab.Sleep(15000);
	              if (login.backupProgressPageExist()) {
	              	logInfo.pass("Analyzing Screen automatically closed after directory analysis and BackupProgress Page Exist");
	                ab.Sleep(20000);
	              } else {
	                 	logInfo.fail("BackUp Page Does Not Exist and Device was Not Added");
	              }
	          } else {
	        	  logInfo.pass("Analyzing Page Does Not Exist");
	        	  Assert.fail();
	          }
	           
	       } else {
	       	logInfo.fail("Home Page does not exists");

	       }
			 } catch (AssertionError | Exception e) {
		     		testStepHandler("FAIL",logInfo,e);
		     	}
	    }



}
