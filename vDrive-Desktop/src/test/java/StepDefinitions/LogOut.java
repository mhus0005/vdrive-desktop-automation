package StepDefinitions;





import org.sikuli.script.Pattern;

import org.sikuli.script.Screen;

import com.aventstack.extentreports.ExtentTest;


import managers.Driver;
import managers.Log;
import screens.AbstractScreen;



public class LogOut extends AbstractScreen {
	
	

	public static ExtentTest test;
	
	private Pattern openicon;
    private Pattern vDriveIcon;
    private Pattern Threedots;
    private Pattern quitButton;
    private Pattern logOutButton;
    private Pattern blueTick;
    private Pattern greenTick;
  //  private Pattern syncFile;
    private Pattern upTodateSync;
    private Pattern resetquitButton;
    private Pattern syncTime;
    private Pattern settingsButton;
    private Pattern startIcon;
    private Screen driver;
    private Pattern BackUpStartNow;
    private Pattern StartNowButton;
    private Pattern BackUpAndAccess;
    
    public void run() {				

	}


    public LogOut(){
    	openicon = new Pattern("./Resources/images/LogOut/aerrow.PNG");
    	settingsButton = new Pattern("./Resources/images/LogOut/settingsButton.PNG");
        vDriveIcon = new Pattern("./Resources/images/LogOut/cloud.PNG");
        Threedots = new Pattern("./Resources/images/LogOut/dots.PNG");
        quitButton = new Pattern("./Resources/images/LogOut/quitButton.PNG");
        resetquitButton = new Pattern("./Resources/images/LogOut/r&qButton.PNG");
        logOutButton = new Pattern("./Resources/images/LogOut/logOut.PNG");
        startIcon = new Pattern("./Resources/images/LogOut/startIcon.PNG");
        blueTick = new Pattern("./Resources/images/LogOut/blueTick.PNG");
        greenTick = new Pattern("./Resources/images/LogOut/greenTick.PNG");
  //      syncFile = new Pattern("./Resources/images/LogOut/syncingFile.PNG");
        upTodateSync = new Pattern("./Resources/images/LogOut/upDate.PNG");
        syncTime = new Pattern("./Resources/images/LogOut/syncTime.PNG");
        BackUpStartNow = new Pattern("./Resources/images/LogInPage/BackUpStartNow.PNG");
        StartNowButton = new Pattern("./Resources/images/LogInPage/StartNowButton.PNG");
        BackUpAndAccess = new Pattern("./Resources/images/LogInPage/BackUpAndAccess.PNG");

    }


    public boolean upTodateSyncExist() {
        driver = getScreenDriver();
        return find(driver, upTodateSync.exact());
    }
    
    public boolean BackUpStartNowMessageExist() {
        driver = Driver.getInstance();
        return   find(driver, BackUpStartNow.exact());
    }
    
    public boolean syncTimeExist() {
        driver = getScreenDriver();
        return find(driver, syncTime.exact());
    }
    
   
    public LogOut clickOpeniconButton() {
        driver = Driver.getInstance();
        click(driver, openicon.exact());
        return this;
    }
    
    public boolean vDriveIconExist() {
        driver = getScreenDriver();
        return find(driver, vDriveIcon.exact());
    }
    
    public boolean BlueSyncExist() {
        driver = getScreenDriver();
        return find(driver, blueTick.exact());
    }
 
    
    public LogOut OpenvDriveIcon() {
        driver = Driver.getInstance();
        click(driver, vDriveIcon.exact());
        return this;
    }
    
    public LogOut OpenSystrayIcon() {
        driver = Driver.getInstance();
        click(driver, openicon.exact());
        return this;
    }
    
    public LogOut ClickOnDotsManu() {
        driver = Driver.getInstance();
        click(driver, Threedots.exact());
        return this;
    }
    
    public LogOut ClickBlueSync() {
        driver = Driver.getInstance();
        click(driver, blueTick.exact());
        return this;
    }
    
    public LogOut ClickGreenSync() {
        driver = Driver.getInstance();
        click(driver, greenTick.exact());
        return this;
    }
    
    public LogOut hoverMouse() {
        driver = Driver.getInstance();
        hover(driver,startIcon);
        return this;
    }
    
    public LogOut OpenToolBarManu() {
        driver = Driver.getInstance();
        click(driver, vDriveIcon.exact());
        click(driver, Threedots.exact());
        return this;
    }
    
    public boolean LogOutButtonExist() {
        driver = getScreenDriver();
        return find(driver, logOutButton.exact());
    }
    
    public boolean QuitButtonExist() {
        driver = getScreenDriver();
        return find(driver, quitButton.exact());
    }
    
    public boolean SettingButtonExist() {
        driver = getScreenDriver();
        return find(driver, settingsButton.exact());
    }
    
    
    
    
    public boolean greenSyncExist() {
        driver = getScreenDriver();
        return find(driver, greenTick.exact());
    }
    

    public LogOut verifySync() {
    	AbstractScreen ab = new AbstractScreen();
        try {
			ab.Sleep(15000);
			click(driver, openicon.exact());
	        System.out.println("Sync will start Now");
			 do {
		    	 hover(driver,startIcon);
		    	 ab.Sleep(8000);
		  	     click(driver, openicon);
		  	     hover(driver,startIcon);
		  	     ab.Sleep(8000);
		  	     click(driver, openicon);
		  	    System.out.println("Still Blue");
		    	 
		     } while (find(driver, blueTick)); 
	 
			   if (find(driver, greenTick.exact())) {
			   	 System.out.println("It Is Green");
			   	 hover(driver,startIcon);
			 	     ab.Sleep(4000);
			 	     click(driver, openicon);
			 	     hover(driver,startIcon);
			 	    ab.Sleep(3000);
			    } else {
			    	System.out.println("It Is Not Green Yet");
			    }
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        

       return this;
    }
    

    
    public LogOut QuitApp() throws InterruptedException {
    		
    	AbstractScreen ab = new AbstractScreen();
    	 ab.Sleep(6000);
    	 click(driver, openicon.exact());
         ab.Sleep(3000);
         click(driver, vDriveIcon.exact());
         ab.Sleep(3000);
         click(driver, Threedots.exact());
         ab.Sleep(3000);
         click(driver, quitButton.exact());
         System.out.println("LogOffed Successfully");
         
         return this;
    }
    
    public LogOut ResetWithQuitApp() throws InterruptedException {
		
    	AbstractScreen ab = new AbstractScreen();
    	 ab.Sleep(6000);
    	 click(driver, openicon.exact());
         ab.Sleep(3000);
         click(driver, vDriveIcon.exact());
         ab.Sleep(3000);
         click(driver, Threedots.exact());
         ab.Sleep(3000);
         click(driver, resetquitButton.exact());
         System.out.println("LogOffed Successfully");
         
         return this;
    }
    
    
    public LogOut closeExistingApp() throws InterruptedException {
    	driver = Driver.getInstance();
    	AbstractScreen ab = new AbstractScreen();
        ab.Sleep(5000);
        click(driver, openicon.exact());
        if(find(driver, blueTick.exact())){
            click(driver, blueTick.exact());
            System.out.println("Its Blue Tick");
            Log.info("Its Blue Tick");
            ab.Sleep(3000);
            click(driver, Threedots.exact());
            ab.Sleep(3000);
            click(driver, logOutButton);
            ResetWithQuitApp();
        } else if(find(driver, greenTick.exact())) {
            click(driver, greenTick.exact());
            System.out.println("Its Green Tick");
            ab.Sleep(3000);
            click(driver, Threedots.exact());
            ab.Sleep(3000);
            click(driver, logOutButton);
            ResetWithQuitApp();
       } else if (find(driver,vDriveIcon.exact())){
            click(driver, vDriveIcon.exact());
            System.out.println("Its regular Colud Tick");
            ab.Sleep(3000);
            click(driver, Threedots.exact());
            ab.Sleep(3000);
            click(driver, logOutButton);
            ResetWithQuitApp();
       
        } else {
        	System.out.println("Application is Closed");

        }
        
        return this;
    }
    
    
    
    
    public LogOut clickLogOff() throws InterruptedException {
        driver = Driver.getInstance();
        if(find(driver, openicon.exact())){
            AbstractScreen ab = new AbstractScreen();
            ab.Sleep(8000);
            click(driver, openicon.exact());
            if(find(driver, blueTick.exact())){
                click(driver, blueTick.exact());
                System.out.println("Its Blue Tick");
                Log.info("Its Blue Tick");
                ab.Sleep(3000);
                click(driver, Threedots.exact());
                ab.Sleep(3000);
                click(driver, logOutButton);
              
            } else if(find(driver, greenTick.exact())) {
                click(driver, greenTick.exact());
                System.out.println("Its Green Tick");
                ab.Sleep(3000);
                click(driver, Threedots.exact());
                ab.Sleep(3000);
                click(driver, logOutButton);
               
           } else { click(driver, vDriveIcon.exact());
                System.out.println("Its regular Colud Tick");
                ab.Sleep(3000);
                click(driver, Threedots.exact());
                ab.Sleep(3000);
                click(driver, logOutButton);
               
            }

        }
        return this;
    }
}
