package StepDefinitions;


import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;

import Listeners.ExtentReportListener;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import screens.AbstractScreen;
import screens.LogInScreen;

public class NoVerizonCloudAccountWarning14 extends ExtentReportListener{
	
	
	Hooks hk = new Hooks();
	AbstractScreen ab = new AbstractScreen();
	LogInScreen login = new LogInScreen();
	
	 @Given("^Open vDriver Desktop Application For invalid password Test$")
	    public void open_vdriver_desktop_application_for_invalid_password_test() throws Throwable {
		 ExtentTest logInfo=null;
			try {	
			test = extent.createTest(Feature.class, "No Verizon Cloud Account Warning-vDrive14");
			test=test.createNode(Scenario.class, "No Verizon Cloud Account Warning:Enter correct MDN but invalid password - 01");
			logInfo=test.createNode(new GherkinKeyword("Given"),"Open_vdriver_desktop_application_for_invalid_password_test");
	        
			hk.loginToTheApp();
		     ab.Sleep(5000);
		     if (login.isLoginWindowExist()){
		    	  
		        	logInfo.pass("User already in LogIn screen");
		 
		        } else if (login.isresetLoginWindowExist()) {
		        	logInfo.pass("User already in LogIn screen");
		        }
		        else if(login.VdriveContinueButtonExist()) {
		        	
		            login.clickVdriveContinueButton();
		            logInfo.info("It clikc on Continue Verizon Button");
		            ab.Sleep(10000);
		            if (login.isLoginWindowExist()) {
		            logInfo.pass("User is in LogIn screen");

		            } else {
		            logInfo.fail("Login screen does not exist");;

		            } 
				
			} 
			}catch (AssertionError | Exception e) {
	    		testStepHandler("FAIL",logInfo,e);
	    	}

	    }
	 
	 
	    @Given("^Open vDriver Desktop Application disable the network Test$")
	    public void open_vdriver_desktop_application_disable_the_network_test() throws Throwable {
	    	 ExtentTest logInfo=null;
				try {
			test = extent.createTest(Feature.class, "No Verizon Cloud Account Warning-vDrive14");		
	    	test=test.createNode(Scenario.class, "Scenario: No Verizon Cloud Account Warning:Enter valid VZW login details and disable the network - 02");
	    	logInfo=test.createNode(new GherkinKeyword("Given"),"Open_vdriver_desktop_application_disable_the_network_test");
	    	
	    	hk.loginToTheApp();
		    ab.Sleep(5000);
		    if (login.isLoginWindowExist()){
		    	  
	        	logInfo.pass("User already in LogIn screen");
	 
	        } else if (login.isresetLoginWindowExist()) {
	        	logInfo.pass("User already in LogIn screen");
	        }
	        else if(login.VdriveContinueButtonExist()) {
	        	
	            login.clickVdriveContinueButton();
	            logInfo.info("It clikc on Continue Verizon Button");
	            ab.Sleep(10000);
	            if (login.isLoginWindowExist()) {
	            logInfo.pass("User is in LogIn screen");

	            } else {
	            logInfo.fail("Login screen does not exist");;

	            } 
			
		}
				} catch (AssertionError | Exception e) {
		    		testStepHandler("FAIL",logInfo,e);
		    	}
	    }

	    @When("^User enters correct \"([^\"]*)\" and invalid \"([^\"]*)\" and \"([^\"]*)\" click on Sign in button$")
	    public void user_enters_correct_something_and_invalid_something_click_on_sign_in_button(String strArg1, String strArg2, String strArg3) throws Throwable {
	    	ExtentTest logInfo=null;
			try {
				logInfo=test.createNode(new GherkinKeyword("When"),"User_enters_correct_'UserID'_and_invalid_'Password'_click_on_Sign_in_button");
	    
	            AbstractScreen ab = new AbstractScreen();
	            LogInScreen login = new LogInScreen();
	            ab.Sleep(8000);
	            logInfo.info("User ID and invalid Password will be provided");
	            if (login.isLoginWindowExist()) {
	                login.enterLoginData(strArg1, strArg2);
	                if(login.secterQusPageExist()) {
                        login.provideSecretQus(strArg3);
                        login.clickSecretContinueButton();
                        ab.Sleep(15000); 
                        logInfo.info("Asked For Secret Qus");
                    }
	                logInfo.info("User Credentials entered");
	                logInfo.info("User correct UserID and invalid Password was entered");
	                login.clickLogIn();
	                
	            }  else if (login.isresetLoginWindowExist()){
	            	login.enterresetLoginData(strArg1, strArg2);
	            	if(login.secterQusPageExist()) {
                        login.provideSecretQus(strArg3);
                        login.clickSecretContinueButton();
                        ab.Sleep(15000); 
                        logInfo.info("Asked For Secret Qus");
                    }
	                logInfo.info("User Credentials entered");
	                logInfo.info("User correct UserID and invalid Password was entered");
	                login.clickresetLogIn();
	                ab.Sleep(5000); }
	            
	            else {

	           	logInfo.info("LogIn Page did not Exist Test Case Failed");
	                Assert.fail();
	            }
			} catch (AssertionError | Exception e) {
	    		testStepHandler("FAIL",logInfo,e);
	    	}
	      
	    }

	    @When("^User enters valid \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" for Login and Verify an error message Unable to access the network$")
	    public void user_enters_valid_something_and_something_and_something_for_login_and_verify_an_error_message_unable_to_access_the_network(String strArg1, String strArg2,  String strArg3) throws Throwable {
	    	ExtentTest logInfo=null;
			try {
				logInfo=test.createNode(new GherkinKeyword("When"),"user_enters_valid_UserID_and_Password_for_login_and_verify_an_error_message_unable_to_access_the_network");
	            AbstractScreen ab = new AbstractScreen();
	            LogInScreen login = new LogInScreen();
	            ab.Sleep(8000);
	            logInfo.info("User ID and Password will be provided");
	            if (login.isLoginWindowExist()) {
	                login.enterLoginData(strArg1, strArg2);
	                logInfo.info("Please Check Network is Disable");
	                ab.Sleep(18000);
	                login.clickLogIn();
	                if(login.secterQusPageExist()) {
                        login.provideSecretQus(strArg3);
                        login.clickSecretContinueButton();
                        ab.Sleep(15000); 
                        logInfo.info("Asked For Secret Qus");
                    }
	                if (login.ErrorCode106MsgExist()) {
	                	logInfo.pass("Error message 'Unable to access the network.(Error code:106)' Displayed");
		            } else {
		            	logInfo.fail("Error message 'Unable to access the network.(Error code:106)' Did Not Displayed");
		            }

	            } else if (login.isresetLoginWindowExist()) {
	                login.enterresetLoginData(strArg1, strArg2);
	                logInfo.info("Please Check Network is Disable");
	                ab.Sleep(18000);
	                login.clickresetLogIn();
	                if(login.secterQusPageExist()) {
                        login.provideSecretQus(strArg3);
                        login.clickSecretContinueButton();
                        ab.Sleep(15000); 
                        logInfo.info("Asked For Secret Qus");
                    }
	                if (login.ErrorCode106MsgExist()) {
	                	logInfo.pass("Error message 'Unable to access the network.(Error code:106)' Displayed");
		            } else {
		            	logInfo.fail("Error message 'Unable to access the network.(Error code:106)' Did Not Displayed");
		            }

	            } else {
	           	logInfo.info("LogIn Page did not Exist Test Case Failed");
	                Assert.fail();
	            }
			} catch (AssertionError | Exception e) {
	    		testStepHandler("FAIL",logInfo,e);
	    	}
	    }

	    @And("^Close vDriver Application vDrive14$")
	    public void close_vdriver_application_vdrive14() throws Throwable {
	    	ExtentTest logInfo=null;
	    	LogOut logOff = new LogOut();
			try {
			logInfo=test.createNode(new GherkinKeyword("Then"),"close_vdriver_application_vdrive14");
	    	hk.logOff();
	    	logOff.ResetWithQuitApp();
	    } catch (AssertionError | Exception e) {
    		testStepHandler("FAIL",logInfo,e);
    	}
	    }
	

	    @Then("^Verify an error message 'The information you entered does not match the information we have on file'$")
	    public void verify_an_error_message_the_information_you_entered_does_not_match_the_information_we_have_on_file() throws Throwable {
	    	ExtentTest logInfo=null;
			try {
			logInfo=test.createNode(new GherkinKeyword("Then"),"verify_an_error_message_the_information_you_entered_does_not_match_the_information_we_have_on_file");
	    	AbstractScreen ab = new AbstractScreen();
            LogInScreen login = new LogInScreen();
            ab.Sleep(8000);
            if(login.invalidInfoPageExist()) {
           	logInfo.pass("Login Failed and Invalid information Page Displayed");
            }
            else {

            	logInfo.fail("Invalid information Page Did Not Displayed");
   
            }
			 } catch (AssertionError | Exception e) {
		    		testStepHandler("FAIL",logInfo,e);
		    	}
	       
	    }

}
