package StepDefinitions;

import org.junit.runner.RunWith;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;

import Listeners.ExtentReportListener;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.junit.Cucumber;
import screens.AbstractScreen;
import screens.LogInScreen;



@RunWith(Cucumber.class)
public class SetupComleteteFinishWithHappyFlow23 extends ExtentReportListener{
	
	

	Hooks hk = new Hooks();
	
   
    @Given("^Open Application and User is on Login screen$")
    public void open_application_and_user_is_on_login_screen() throws Throwable {
    	ExtentTest logInfo=null;
    	
    	try {
    	test = extent.createTest(Feature.class, "Setup Comletete Finish With Happy Flow");
        test=test.createNode(Scenario.class, "Successful Setup Comletete Finish With Happy Flow");
        logInfo=test.createNode(new GherkinKeyword("Given"),"Open_application_and_user_is_on_login_screen");
        
        AbstractScreen ab = new AbstractScreen();
        LogInScreen login = new LogInScreen();

        hk.loginToTheApp();
        ab.Sleep(15000);
        if (login.isLoginWindowExist()){
  
        	logInfo.pass("User already in LogIn screen");
 
        } else if (login.isresetLoginWindowExist()) {
        	logInfo.pass("User already in LogIn screen");
        }
        else if(login.VdriveContinueButtonExist()) {
        	
            login.clickVdriveContinueButton();
            logInfo.info("It clikc on Continue Verizon Button");
            ab.Sleep(10000);
            if (login.isLoginWindowExist()) {
            logInfo.pass("User is in LogIn screen");

            } else {
            logInfo.fail("Login screen does not exist");;

            } 
        }
    	} catch (AssertionError | Exception e) {
    		testStepHandler("FAIL",logInfo,e);
    	}
    	
    	
        
    }

    @When("^User enters \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
    public void user_enters_something_and_something_and_something(String strArg1, String strArg2, String strArg3) throws Throwable {
    	ExtentTest logInfo=null;
 
    	 try {
    		 logInfo= test.createNode(new GherkinKeyword("When"),"User_enters_Username_and_Password_and_SecretAns");
        AbstractScreen ab = new AbstractScreen();
        LogInScreen login = new LogInScreen();
        ab.Sleep(8000);
        System.out.println("User ID and Password will be provided");
        if (login.isLoginWindowExist()) {
            login.enterLoginData(strArg1, strArg2);
            logInfo.info("User Credentials entered");
            login.clickLogIn();
            ab.Sleep(15000);
                    if(login.homePageExist()) {
                    	logInfo.pass("LogIn Successful");
                    } else if(login.secterQusPageExist()) {
                        login.provideSecretQus(strArg3);
                        logInfo.pass("Looking for the Secret Ans");
                        login.clickSecretContinueButton();
                        ab.Sleep(15000);

                        
                    } else if(login.verificationPageExist()) {
                    	logInfo.info("Need Verification To Login");
                        login.verificationSendButton();
                        ab.Sleep(15000);
                        if (login.isLoginWindowExist()) {
                            login.enterLoginData(strArg1, strArg2);
                            logInfo.pass("User Credentials entered");
                      //      logInfo.pass("User Logged In");
                            login.clickLogIn(); }
                    }
                    else {
                    	logInfo.fail("User having issue with LogIn");

                        Assert.fail();
                    }

        } else if (login.isresetLoginWindowExist()){
        	login.enterresetLoginData(strArg1, strArg2);
            logInfo.info("User Credentials entered");
            login.clickresetLogIn();
            ab.Sleep(15000);
        	if(login.homePageExist()) {
            	logInfo.pass("LogIn Successful");
            } else if(login.secterQusPageExist()) {
                login.provideSecretQus(strArg3);
                logInfo.pass("Looking for the Secret Ans");
                login.clickSecretContinueButton();
                ab.Sleep(15000);

                
            } else if(login.verificationPageExist()) {
            	logInfo.info("Need Verification To Login");
                login.verificationSendButton();
                ab.Sleep(15000);
                if (login.isLoginWindowExist()) {
                    login.enterLoginData(strArg1, strArg2);
                    logInfo.pass("User Credentials entered");
              //      logInfo.pass("User Logged In");
                    login.clickLogIn(); }
            }
            else {
            	logInfo.fail("User having issue with LogIn");

                Assert.fail();
            }
        }
        	else {
       
        	logInfo.fail("LogIn Page did not Exist Test Case Failed");
 
            Assert.fail();
        }

    	 } catch (AssertionError | Exception e) {
     		testStepHandler("FAIL",logInfo,e);
     		Assert.fail();
     	}
    }  

    

    @And("^User click on Add Device and continue$")
    public void user_click_on_add_device_and_continue() throws Throwable {
    	ExtentTest logInfo=null;
		 try {
			 logInfo= test.createNode(new GherkinKeyword("And"),"User_click_on_add_device_and_continue");
        AbstractScreen ab = new AbstractScreen();
        LogInScreen login = new LogInScreen();
        ab.Sleep(15000);
        if (login.homePageExist()) {
        	logInfo.pass("Home Page Exist");

            login.clickAddDeviceButton();
            ab.Sleep(15000);
            if (login.backupProgressPageExist()) {
            	logInfo.pass("BackupProgress Page Exist");
       
                login.clickbackUpContinueButton();
                logInfo.pass("Device Added successfully");
      
            } else {
            	logInfo.fail("Login Page does not exists");
 
            }
        } else {
        	logInfo.fail("Login Page does not exists");

        }
		 } catch (AssertionError | Exception e) {
	     		testStepHandler("FAIL",logInfo,e);
	     	}
    }

    @And("^Dashboard will display and Verify the happy flow with Green Tick$")
    public void dashboard_will_display_and_verify_the_happy_flow_with_green_tick() throws Throwable {
    	ExtentTest logInfo=null;
	try {
		logInfo= test.createNode(new GherkinKeyword("And"),"Dashboard_will_display_and_verify_the_happy_flow_with_green_tick");
        AbstractScreen ab = new AbstractScreen();
        LogOut logout = new LogOut();
        ab.Sleep(5000);
        logout.verifySync();
        logInfo.pass("Verify Done");
	} catch (AssertionError | Exception e) {
 		testStepHandler("FAIL",logInfo,e);
 	}
    } 
    
    @Then("^Close vDriver Application vDrive23$")
    public void close_vdriver_application_vdrive23() throws Throwable {
    	ExtentTest logInfo=null;
    	LogOut logOff = new LogOut();
    	try {
			 logInfo= test.createNode(new GherkinKeyword("Then"),"Close_vdriver_application");
    	hk.logOff();
    	logOff.ResetWithQuitApp();
    	} catch (AssertionError | Exception e) {
    		testStepHandler("FAIL",logInfo,e);
    	}
    } 

  


}
