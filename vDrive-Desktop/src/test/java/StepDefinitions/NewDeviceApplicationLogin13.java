package StepDefinitions;


import org.junit.runner.RunWith;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;


import Listeners.ExtentReportListener;
import cucumber.api.junit.Cucumber;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import screens.AbstractScreen;
import screens.LogInScreen;

@RunWith(Cucumber.class)
public class NewDeviceApplicationLogin13 extends ExtentReportListener{
		
		
		Hooks hk = new Hooks();
		AbstractScreen ab = new AbstractScreen();
		LogInScreen login = new LogInScreen();
		
		
			@Given("^Open vDriver Desktop Application For Onboarding screen$")
	    	public void open_vdriver_desktop_application_for_onboarding_screen() throws Throwable {
				ExtentTest logInfo=null;
			try {	
				test = extent.createTest(Feature.class, "New Device Application Login-vDrive13");
			    test=test.createNode(Scenario.class, "Take user to first onboarding screen/continue with VZ.");
			    logInfo=test.createNode(new GherkinKeyword("Given"),"open_vdriver_desktop_application_for_onboarding_screen");
			    
				hk.loginToTheApp();
		      //  ab.Sleep(5000);
			} catch (AssertionError | Exception e) {
	    		testStepHandler("FAIL",logInfo,e);
	    	}
	    	}
			
			@Given("^Open vDriver Desktop Application For First time user$")
			public void open_vdriver_desktop_application_for_first_time_user() throws Throwable {
				ExtentTest logInfo=null;
				try {
				test = extent.createTest(Feature.class, "New Device Application Login-vDrive13");	
				test=test.createNode(Scenario.class, "Scenario: First time user login flow upto V-Drive Start-Up screen");
				logInfo=test.createNode(new GherkinKeyword("Given"),"open_vdriver_desktop_application_for_first_time_user");

				hk.loginToTheApp();
		  //      ab.Sleep(5000);
		      
				} catch (AssertionError | Exception e) {
		    		testStepHandler("FAIL",logInfo,e);
		    	}
			}
			
			
			@Given("^Open vDriver Desktop Application For login window for 2nd time$")
		    public void open_vdriver_desktop_application_for_login_window_for_2nd_time() throws Throwable {
		    	ExtentTest logInfo=null;
				try {
				test = extent.createTest(Feature.class, "New Device Application Login-vDrive13");	
		    	test=test.createNode(Scenario.class, "Scenario: Take user to the VZ login window for 2nd or higher time login");
		    	logInfo=test.createNode(new GherkinKeyword("Given"),"Open_vdriver_desktop_application_for_login_window_for_2nd_time");
		    	hk.loginToTheApp();
		     //   ab.Sleep(5000);
		    } catch (AssertionError | Exception e) {
	    		testStepHandler("FAIL",logInfo,e);
	    	}
		    }
        
	
		   
		    @Then("^Verifying Onboarding screen and continue Button for vDriver Desktop Application$")
		    public void verifying_onboarding_screen_and_continue_button_for_vdriver_desktop_application() throws Throwable {
		    	ExtentTest logInfo=null;
				try {
				logInfo=test.createNode(new GherkinKeyword("Then"),"Verifying_onboarding_screen_and_continue_button_for_vdriver_desktop_application");

		    //	ab.Sleep(5000);
		    	
		    	if (login.isvDriveOnboardingPageExist()) {
		    		logInfo.pass("The Verizon Drive Onboarding screen Exist");
                	login.clickVdriveContinueButton();
                	if (login.isLoginWindowExist()) {
                		logInfo.info("User is in LogIn screen");
  		            } else if (login.isresetLoginWindowExist()){
  		            	logInfo.info("User is in LogIn screen");
  		            }
  		            else {
  		            	logInfo.info("Login screen does not exists");
  		            } 
                } else {
                	logInfo.info("The Verizon Drive Onboarding screen Does Not Exist");
		           Assert.fail();
                }
		    	
				 } catch (AssertionError | Exception e) {
			    		testStepHandler("FAIL",logInfo,e);
			    	}
		    }
		    
		    @When("^Verify Onboarding screen did not displayed but Only Login Page will Display$")
		    public void verify_onboarding_screen_did_not_displayed_but_only_login_page_will_display() throws Throwable {
		    	ExtentTest logInfo=null;
				try {
				logInfo=test.createNode(new GherkinKeyword("When"),"Verify_onboarding_screen_did_not_displayed_but_only_login_page_will_display");
		    	ab.Sleep(5000);
		    	LogInScreen login = new LogInScreen();
		    	if (login.isLoginWindowExist()) {
		    		login.clickLogInPageExist();
  		           logInfo.pass("It Directly Opend LogIn screen Displayed");
  		            } else if (login.isresetLoginWindowExist()){
  		            	logInfo.info("User is in LogIn screen");
  		            } else {

  		           logInfo.fail("Login screen does not exists");
  		             Assert.fail();
  		            } 
		    } catch (AssertionError | Exception e) {
	    		testStepHandler("FAIL",logInfo,e);
	    	}
		    }
		    
		    @When("^Click on Continue Button and Open Login Screen$")
		    public void click_on_continue_button_and_open_login_screen() throws Throwable {
		    	ExtentTest logInfo=null;
				try {
				logInfo=test.createNode(new GherkinKeyword("When"),"Click_on_continue_button_and_open_login_screen");
		   // 	ab.Sleep(5000);
		    	LogInScreen login = new LogInScreen();
		    	try {
                	 if (login.isvDriveOnboardingPageExist()) {
 	                	logInfo.pass("The Verizon Drive Onboarding screen Exist");
 	                	login.clickVdriveContinueButton();
 	                	if (login.isLoginWindowExist()) {
 	  		             logInfo.pass("User is in LogIn screen");
 	  		            } else if (login.isresetLoginWindowExist()){
 	  		            	logInfo.info("User is in LogIn screen");
 	  		            } else {
 	  		            	logInfo.fail("LogIn screen Did Not Opened");
 	  		            } 
 	                } else {
 	                  logInfo.fail("The Verizon Drive Onboarding screen Does Not Exist");
	  		           Assert.fail();
 	                }
                }
                catch (Exception e) {
                    e.printStackTrace();

                    Assert.fail();
                } 
				 } catch (AssertionError | Exception e) {
			    		testStepHandler("FAIL",logInfo,e);
			    	}
		    }
		    
		    
		    @And("^User enters \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" for Login$")
		    public void user_enters_something_and_something_and_something_for_login(String strArg1, String strArg2, String strArg3) throws Throwable {
		    	ExtentTest logInfo=null;
				try {
				logInfo=test.createNode(new GherkinKeyword("And"),"User_enters_Username_and_Password_and_SecretAns_if_required");
		    	try {
		            AbstractScreen ab = new AbstractScreen();
		            LogInScreen login = new LogInScreen();
		            ab.Sleep(5000);
		            logInfo.info("User ID and Password will be provided");
		            if (login.isLoginWindowExist()) {
		                login.enterLoginData(strArg1, strArg2);
		              
		                logInfo.info("User Credentials entered");
		                login.clickLogIn();
		                ab.Sleep(10000);
		                        if(login.homePageExist()) {
		                       	
		                       	 logInfo.pass("LogIn Successful");
		                        } else if(login.secterQusPageExist()) {
		                            login.provideSecretQus(strArg3);
		                            login.clickSecretContinueButton();
		                            ab.Sleep(15000);
		                           
		                            logInfo.pass("User Logged In");
		                        } else if(login.verificationPageExist()) {
		                            login.verificationSendButton();
		                            ab.Sleep(15000);
		                            if (login.isLoginWindowExist()) {
		                                login.enterLoginData(strArg1, strArg2);
		                              
		                                logInfo.pass("User Credentials entered");
		                                login.clickLogIn(); }
		                        }
		                        else {
		                       	 
		                       	logInfo.fail("User having issue with LogIn");
		                            Assert.fail();
		                        }

		            }else if (login.isresetLoginWindowExist()){
		            	login.enterresetLoginData(strArg1, strArg2);
		                logInfo.info("User Credentials entered");
		                login.clickresetLogIn();
		                ab.Sleep(15000);
		            	if(login.homePageExist()) {
		                	logInfo.pass("LogIn Successful");
		                } else if(login.secterQusPageExist()) {
		                    login.provideSecretQus(strArg3);
		                    logInfo.pass("Looking for the Secret Ans");
		                    login.clickSecretContinueButton();
		                    ab.Sleep(15000);

		                    
		                } else if(login.verificationPageExist()) {
		                	logInfo.info("Need Verification To Login");
		                    login.verificationSendButton();
		                    ab.Sleep(15000);
		                    if (login.isLoginWindowExist()) {
		                        login.enterLoginData(strArg1, strArg2);
		                        logInfo.pass("User Credentials entered");
		                  //      logInfo.pass("User Logged In");
		                        login.clickLogIn(); }
		                }
		                else {
		                	logInfo.fail("User having issue with LogIn");

		                    Assert.fail();
		                }
		            } else {
		           	
		           	logInfo.fail("LogIn Page did not Exist Test Case Failed");
		                Assert.fail();
		            }
		        } catch (Exception e) {
		            e.printStackTrace();
		            
		            Assert.fail();
		        }  
		    } catch (AssertionError | Exception e) {
	    		testStepHandler("FAIL",logInfo,e);
	    	}
		    }
		    
		    
		    @And("^Verify vDrive Startup screen$")
		    public void verify_vdrive_startup_screen() throws Throwable {
		    	ExtentTest logInfo=null;
				try {
				logInfo=test.createNode(new GherkinKeyword("And"),"Verify_vdrive_startup_screen");
		    	try {
		            AbstractScreen ab = new AbstractScreen();
		            LogInScreen login = new LogInScreen();
		            ab.Sleep(15000);
		            if (login.homePageExist()) {

		           	logInfo.pass("Start-Up screen Exist");
		            } else {
		           	 logInfo.fail("Start-Up screen Does Not Exist");
		           	Assert.fail();
		            }
		        } catch (Exception e) {
		            e.printStackTrace();
		          
		            Assert.fail();
		        } 
		    } catch (AssertionError | Exception e) {
	    		testStepHandler("FAIL",logInfo,e);
	    	}
		    }

		    @Then("^Close vDriver Application First vDrive13$")
		    public void close_vdriver_application_first_vdrive13() throws Throwable {
		    	ExtentTest logInfo=null;
		    	LogOut logOff = new LogOut();
				try {
				logInfo=test.createNode(new GherkinKeyword("Then"),"Close_vdriver_application");
		    	hk.logOff();
		    	logOff.QuitApp();
		    } catch (AssertionError | Exception e) {
	    		testStepHandler("FAIL",logInfo,e);
	    	}
		    }
		  

		    @Then("^Close vDriver Application vDrive13$")
		    public void close_vdriver_application_vdrive13() throws Throwable {
		    	ExtentTest logInfo=null;
		    	LogOut logOff = new LogOut();
				try {
				logInfo=test.createNode(new GherkinKeyword("Then"),"Close_vdriver_application_vdrive13");
		    	hk.logOff();
		    	logOff.ResetWithQuitApp();
		    } catch (AssertionError | Exception e) {
	    		testStepHandler("FAIL",logInfo,e);
	    	}
		    }

}
