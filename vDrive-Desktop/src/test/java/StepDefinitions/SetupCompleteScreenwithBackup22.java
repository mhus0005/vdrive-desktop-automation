package StepDefinitions;

import org.junit.runner.RunWith;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;

import Listeners.ExtentReportListener;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.junit.Cucumber;
import screens.AbstractScreen;
import screens.LogInScreen;

@RunWith(Cucumber.class)
public class SetupCompleteScreenwithBackup22 extends ExtentReportListener{
	
	Hooks hk = new Hooks();
	
	 @Given("^Open vDriver Desktop Application For Start-up Screen BackUp$")
	    public void open_vdriver_desktop_application_for_startup_screen_backup() throws Throwable {
		 ExtentTest logInfo=null;
	    	
	    	try {
	    	test = extent.createTest(Feature.class, "Setup Complete Screen with Backup-vDrive-22");
	        test=test.createNode(Scenario.class, "Setup Complete Screen with Backup:Click on 'Add device' and verify Continue Button");
	        logInfo=test.createNode(new GherkinKeyword("Given"),"Open_vdriver_desktop_application_for_startup_screen_backup");
	        
	        AbstractScreen ab = new AbstractScreen();
	        LogInScreen login = new LogInScreen();

	        hk.loginToTheApp();
	        ab.Sleep(15000);
	        if (login.isLoginWindowExist()){
	  
	        	logInfo.pass("User already in LogIn screen");
	 
	        } else if (login.isresetLoginWindowExist()) {
	        	logInfo.pass("User already in LogIn screen");
	        }
	        else if(login.VdriveContinueButtonExist()) {
	        	
	            login.clickVdriveContinueButton();
	            logInfo.info("It click on Continue Verizon Button");
	            ab.Sleep(10000);
	            if (login.isLoginWindowExist()) {
	            logInfo.pass("User is in LogIn screen");
	            } else if (login.isresetLoginWindowExist()){
	            	logInfo.pass("User is in LogIn screen");
	            } else {
	            logInfo.fail("Login screen does not exist");
	            

	            } 
	        }
	    	} catch (AssertionError | Exception e) {
	    		testStepHandler("FAIL",logInfo,e);
	    	}
	    	
	    	
	        
	       
	    
	    }

	    @When("^Enter \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" for login$")
	    public void enter_something_and_something_and_something_for_login(String strArg1, String strArg2, String strArg3) throws Throwable {

	    	ExtentTest logInfo=null;
	    	 
	   	 try {
	   		 logInfo= test.createNode(new GherkinKeyword("When"),"Enter_Username_and_Password_and_SecretAns__for_login");
	       AbstractScreen ab = new AbstractScreen();
	       LogInScreen login = new LogInScreen();
	       ab.Sleep(8000);
	       System.out.println("User ID and Password will be provided");
	       if (login.isLoginWindowExist()) {
	           login.enterLoginData(strArg1, strArg2);
	           logInfo.info("User Credentials entered");
	           login.clickLogIn();
	           ab.Sleep(15000);
	                   if(login.homePageExist()) {
	                   	logInfo.pass("LogIn Successful");
	                   } else if(login.secterQusPageExist()) {
	                       login.provideSecretQus(strArg3);
	                       logInfo.pass("Looking for the Secret Ans");
	                       login.clickSecretContinueButton();
	                       ab.Sleep(15000);

	                       
	                   } else if(login.verificationPageExist()) {
	                   	logInfo.info("Need Verification To Login");
	                       login.verificationSendButton();
	                       ab.Sleep(15000);
	                       if (login.isLoginWindowExist()) {
	                           login.enterLoginData(strArg1, strArg2);
	                           logInfo.pass("User Credentials entered");
	                           login.clickLogIn(); }
	                   }
	                   else {
	                   	logInfo.fail("User having issue with LogIn");
	                   	Assert.fail("User having issue with LogIn");	
	                   }

	       } else if (login.isresetLoginWindowExist()){
	       	login.enterresetLoginData(strArg1, strArg2);
	           logInfo.info("Reset User Credentials entered");
	           login.clickresetLogIn();
	           ab.Sleep(20000);
	       	if(login.homePageExist()) {
	           	logInfo.pass("LogIn Successful");
	           } 
	       	else if(login.secterQusPageExist()) {
	               login.provideSecretQus(strArg3);
	               logInfo.pass("Looking for the Secret Ans");
	               login.clickSecretContinueButton();
	               ab.Sleep(15000);               
	           } 
	       	else if(login.verificationPageExist()) {
	           	logInfo.info("Need Verification To Login");
	               login.verificationSendButton();
	               ab.Sleep(15000);
	               if (login.isLoginWindowExist()) {
	                   login.enterLoginData(strArg1, strArg2);
	                   logInfo.pass("User Credentials entered");
	                   login.clickLogIn(); }
	           }
	           else {
	           	logInfo.fail("User having issue with LogIn");
	           	Assert.fail("User having issue with LogIn");
	           }
	       }
	       	else {
	      
	       	logInfo.fail("LogIn Page did not Exist Test Case Failed");
	       	Assert.fail("LogIn Page did not Exist Test Case Failed");
	       }

	   	 } catch (AssertionError | Exception e) {
	    		testStepHandler("FAIL",logInfo,e);
	    	}
	       
	    }

	    @Then("^User click on Add Device and verify BackUp Page and Continue Button$")
	    public void user_click_on_add_device_and_verify_backup_page_and_continue_button() throws Throwable {
	    	ExtentTest logInfo=null;
			 try {
				 logInfo= test.createNode(new GherkinKeyword("Then"),"User_click_on_add_device_and_verify_backup_page_and_continue_button");
	        AbstractScreen ab = new AbstractScreen();
	        LogInScreen login = new LogInScreen();
	        ab.Sleep(15000);
	        if (login.homePageExist()) {
	        	logInfo.pass("Home Page Exist");

	            login.clickAddDeviceButton();
	            ab.Sleep(15000);
	            if (login.backupProgressPageExist()) {
	            	logInfo.pass("BackupProgress Page Exist");
	            	
	            	if (login.backUpContinueButtonExist()) {
	            		logInfo.pass("Continue Button In BackUp Page Exist");
	            		ab.Sleep(13000);
	            	} else {
	            		logInfo.fail("Continue Button In BackUp Page Does Not Exist");
	            	}
	      
	            } else {
	            	logInfo.fail("BackupProgress Page does not exists");
	 
	            }
	        } else {
	        	logInfo.fail("Login Page does not exists");

	        }
			 } catch (AssertionError | Exception e) {
		     		testStepHandler("FAIL",logInfo,e);
		     	}
	        
	    }

	    @And("^Close vDriver Application vDrive22$")
	    public void close_vdriver_application_vdrive22() throws Throwable {
	    	ExtentTest logInfo=null;
	    	LogOut logOff = new LogOut();
	    	try {
				 logInfo= test.createNode(new GherkinKeyword("And"),"Close_vdriver_application");
	    	hk.logOff();
	    	logOff.ResetWithQuitApp();
	    	} catch (AssertionError | Exception e) {
	    		testStepHandler("FAIL",logInfo,e);
	    	}
	       
	    }

}
