package StepDefinitions;

import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;

import Listeners.ExtentReportListener;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import screens.AbstractScreen;
import screens.LogInScreen;

public class vDriveSyncStatusforBackedUpDevice48 extends ExtentReportListener{
	
	
	Hooks hk = new Hooks();
	
	@Given("^Open Application and User is on Login screen for Sync Backed Up$")
    public void open_application_and_user_is_on_login_screen_for_sync_backed_up() throws Throwable {
ExtentTest logInfo=null;
    	
    	try {
    	test = extent.createTest(Feature.class, " V-Drive Sync Status for Backed Up Device-vDrive-48");
        test=test.createNode(Scenario.class, "Verify V-Drive Sync Status for Backed Up");
        logInfo=test.createNode(new GherkinKeyword("Given"),"Open_application_and_user_is_on_login_screen_for_sync_backed_up");
        
        AbstractScreen ab = new AbstractScreen();
        LogInScreen login = new LogInScreen();

        hk.loginToTheApp();
        ab.Sleep(15000);
        if (login.isLoginWindowExist()){
  
        	logInfo.pass("User already in LogIn screen");
 
        } else if (login.isresetLoginWindowExist()) {
        	logInfo.pass("User already in LogIn screen");
        }
        else if(login.VdriveContinueButtonExist()) {
        	
            login.clickVdriveContinueButton();
            logInfo.info("It clikc on Continue Verizon Button");
            ab.Sleep(10000);
            if (login.isLoginWindowExist()) {
            logInfo.pass("User is in LogIn screen");

            } else {
            logInfo.fail("Login screen does not exist");;

            } 
        }
    	} catch (AssertionError | Exception e) {
    		testStepHandler("FAIL",logInfo,e);
    	}
    	
    	
     
    }

    @When("^User enters \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" for Sync Backed Up$")
    public void user_enters_something_and_something_and_something_for_sync_backed_up(String strArg1, String strArg2, String strArg3) throws Throwable {
    	ExtentTest logInfo=null;
    	 
   	 try {
   		 logInfo= test.createNode(new GherkinKeyword("When"),"User_enters_Username_and_Password_and_SecretAns_for_sync_backed_up");
       AbstractScreen ab = new AbstractScreen();
       LogInScreen login = new LogInScreen();
       ab.Sleep(8000);
       System.out.println("User ID and Password will be provided");
       if (login.isLoginWindowExist()) {
           login.enterLoginData(strArg1, strArg2);
           logInfo.info("User Credentials entered");
           login.clickLogIn();
           ab.Sleep(15000);
                   if(login.homePageExist()) {
                   	logInfo.pass("LogIn Successful");
                   } else if(login.secterQusPageExist()) {
                       login.provideSecretQus(strArg3);
                       logInfo.pass("Looking for the Secret Ans");
                       login.clickSecretContinueButton();
                       ab.Sleep(15000);

                       
                   } else if(login.verificationPageExist()) {
                   	logInfo.info("Need Verification To Login");
                       login.verificationSendButton();
                       ab.Sleep(15000);
                       if (login.isLoginWindowExist()) {
                           login.enterLoginData(strArg1, strArg2);
                           logInfo.pass("User Credentials entered");
                     //      logInfo.pass("User Logged In");
                           login.clickLogIn(); }
                   }
                   else {
                   	logInfo.fail("User having issue with LogIn");

                       Assert.fail();
                   }

       } else if (login.isresetLoginWindowExist()){
       	login.enterresetLoginData(strArg1, strArg2);
           logInfo.info("User Credentials entered");
           login.clickresetLogIn();
           ab.Sleep(15000);
       	if(login.homePageExist()) {
           	logInfo.pass("LogIn Successful");
           } else if(login.secterQusPageExist()) {
               login.provideSecretQus(strArg3);
               logInfo.pass("Looking for the Secret Ans");
               login.clickSecretContinueButton();
               ab.Sleep(15000);

               
           } else if(login.verificationPageExist()) {
           	logInfo.info("Need Verification To Login");
               login.verificationSendButton();
               ab.Sleep(15000);
               if (login.isLoginWindowExist()) {
                   login.enterLoginData(strArg1, strArg2);
                   logInfo.pass("User Credentials entered");
             //      logInfo.pass("User Logged In");
                   login.clickLogIn(); }
           }
           else {
           	logInfo.fail("User having issue with LogIn");

               Assert.fail();
           }
       }
       	else {
      
       	logInfo.fail("LogIn Page did not Exist Test Case Failed");

           Assert.fail();
       }

   	 } catch (AssertionError | Exception e) {
    		testStepHandler("FAIL",logInfo,e);
    		Assert.fail();
    	}
    }

    @Then("^Close vDriver Application vDrive48$")
    public void close_vdriver_application_vdrive48() throws Throwable {

    	ExtentTest logInfo=null;
    	LogOut logOff = new LogOut();
    	try {
			 logInfo= test.createNode(new GherkinKeyword("Then"),"Close_vdriver_application");
		System.out.println("It will Close the App");	 
    	hk.logOff();
    	logOff.ResetWithQuitApp();
    	} catch (AssertionError | Exception e) {
    		testStepHandler("FAIL",logInfo,e);
    	}
    }

    @And("^User click on Add Device and continue for Sync Backed Up$")
    public void user_click_on_add_device_and_continue_for_sync_backed_up() throws Throwable {
    	ExtentTest logInfo=null;
		 try {
			 logInfo= test.createNode(new GherkinKeyword("And"),"User_click_on_add_device_and_continue_for_sync_backed_up");
       AbstractScreen ab = new AbstractScreen();
       LogInScreen login = new LogInScreen();
       ab.Sleep(15000);
       if (login.homePageExist()) {
       	logInfo.pass("Home Page Exist");

           login.clickAddDeviceButton();
           ab.Sleep(15000);
           if (login.backupProgressPageExist()) {
           	logInfo.pass("BackupProgress Page Exist");
      
               login.clickbackUpContinueButton();
               logInfo.pass("Device Added successfully");
     
           } else {
           	logInfo.fail("Login Page does not exists");

           }
       } else {
       	logInfo.fail("Login Page does not exists");

       }
		 } catch (AssertionError | Exception e) {
	     		testStepHandler("FAIL",logInfo,e);
	     	}
    }

    @And("^Dashboard will display and Verify Sync and Update Status$")
    public void dashboard_will_display_and_verify_sync_and_update_status() throws Throwable {
    	ExtentTest logInfo=null;
    	try {
    		logInfo= test.createNode(new GherkinKeyword("And"),"Dashboard_will_display_and_verify_the_happy_flow_with_green_tick");
            AbstractScreen ab = new AbstractScreen();
            LogOut logout = new LogOut();
            ab.Sleep(5000);
            
            logout.OpenSystrayIcon();
            System.out.println("Clicked On SysTray");
            
            ab.Sleep(15000);
            if(logout.BlueSyncExist()) {
            	logout.ClickBlueSync();
            	if(logout.syncTimeExist()) {
            		logInfo.pass("Sync Time Field Exist and Verified Successfully");
            	} else {
            		logInfo.fail("Sync Time Field Does Not Exist");
            	}
            	logout.OpenSystrayIcon();
            } else {
            	logInfo.info("Blue Sync Does Not Exist");
            	logout.hoverMouse();
                logout.OpenSystrayIcon();
            }
            logout.hoverMouse();
            logout.OpenSystrayIcon();
            System.out.println("Sync will start Now");
            do {
            	logout.hoverMouse();
            	ab.Sleep(4000);
            	logout.OpenSystrayIcon();
            	
            	logout.hoverMouse();
            	logout.OpenSystrayIcon();
            	logInfo.info("Still Blue");
		    	 
		     } while (logout.BlueSyncExist()); 
            
            if (logout.greenSyncExist()) {
            	logInfo.info("It Is Green");
			   	logout.hoverMouse();
			 	ab.Sleep(4000);
			 	logout.OpenSystrayIcon();
			 	logout.hoverMouse();
			 	ab.Sleep(4000);
			 	logout.OpenSystrayIcon();
			 	logout.ClickGreenSync();
			 	if (logout.upTodateSyncExist()) {
			 		logInfo.pass("UpToDate Field Exist and Verified Successfully");
			 		logout.hoverMouse();
			 		ab.Sleep(4000);
				 	logout.OpenSystrayIcon();
				 	logout.hoverMouse();
			 		
			 	} else {
			 		logInfo.fail("UpToDate Field Does Not Exist");
			 		logout.hoverMouse();
			 		ab.Sleep(4000);
				 	logout.OpenSystrayIcon();
			 	}

			    } else {
			    	logInfo.fail("It Is Not Green or Blue");
			    }

    	} catch (AssertionError | Exception e) {
     		testStepHandler("FAIL",logInfo,e);
     	}
    }
}
