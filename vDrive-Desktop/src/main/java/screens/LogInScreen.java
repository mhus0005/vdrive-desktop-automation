package screens;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
//import org.sikuli.script.Region;
//import org.sikuli.script.Region;
import org.sikuli.script.Screen;

import managers.Driver;

public class LogInScreen extends AbstractScreen {
	
    private Pattern username;
    private Pattern password;
    private Pattern signinButton;
    private Pattern addDeviceBT;
    private Pattern loginWindow;
    private Pattern analyzingPage;
    private Pattern bkContinueButton;
    private Pattern backupProgressPage;
    private Pattern homePage;
    private Pattern retryButton;
    private Pattern retryPage;
    private Pattern spinnerLogo;
    private Pattern ContinuewithOutAddingButton;
    private Pattern LogInPageExist;
    
    
    private Pattern resetLogInPage;
    private Pattern resetUserID;
    private Pattern resetPassword;
    private Pattern resetSignInButton;

    private Pattern secretQusPage;
    private Pattern secretQusField;
    private Pattern secretQusContinueButton;
    private Pattern vDriveCloudPage;
    private Pattern vDriveContinueButton;
    private Pattern invalidInfo;
    private Pattern ErrorCode106Msg;
    

    private Pattern SelectFolderTxt;
    private Pattern SelectFolderSave;
    private Pattern SelectAllButton;
    
    private Pattern StartupCloud;
    private Pattern StartupArrowIcon;
    private Pattern StartupContinueButton;

    private Pattern verificationPage;
    private Pattern verificationSendButton;
    private Screen driver;
 //   private Region window;



    


    public LogInScreen(){
        username = new Pattern("./Resources/images/LogInPage/userID.PNG");
        password = new Pattern("./Resources/images/LogInPage/password.PNG");
        signinButton = new Pattern("./Resources/images/LogInPage/signButton.PNG");
        loginWindow = new Pattern("./Resources/images/LogInPage/logInPage.PNG");
        LogInPageExist = new Pattern("./Resources/images/LogInPage/LogInPageExist.PNG");
        
        resetLogInPage = new Pattern("./Resources/images/LogInPage/resetLogInPage.PNG");
        resetUserID = new Pattern("./Resources/images/LogInPage/resetUserID.PNG");
        resetPassword = new Pattern("./Resources/images/LogInPage/resetPassword.PNG");
        resetSignInButton = new Pattern("./Resources/images/LogInPage/resetSignButton.PNG");
        
        ContinuewithOutAddingButton = new Pattern("./Resources/images/LogInPage/ContinuewithOutAddingButton.PNG");
        
        StartupCloud = new Pattern("./Resources/images/LogInPage/StartupCloud.PNG");
        StartupArrowIcon = new Pattern("./Resources/images/LogInPage/StartupArrowIcon.PNG");
        StartupContinueButton = new Pattern("./Resources/images/LogInPage/StartupContinueButton.PNG");

        addDeviceBT = new Pattern("./Resources/images/LogInPage/addDeviceButton.PNG");
        analyzingPage = new Pattern("./Resources/images/LogInPage/AnalyzingPage.PNG");
        backupProgressPage = new Pattern("./Resources/images/LogInPage/backupProgressPage.PNG");
        bkContinueButton = new Pattern("./Resources/images/LogInPage/backupContinueButton.PNG");
        spinnerLogo = new Pattern("./Resources/images/LogInPage/spinner.PNG");
        
        
        SelectFolderTxt = new Pattern("./Resources/images/LogInPage/SelectFolderTxt.PNG");
        SelectFolderSave = new Pattern("./Resources/images/LogInPage/SelectFolderSave.PNG");
        SelectAllButton = new Pattern("./Resources/images/LogInPage/SelectAllButton.PNG");
  
        
        
        retryButton = new Pattern("./Resources/images/LogInPage/retryButton.PNG");
        retryPage = new Pattern("./Resources/images/LogInPage/retryPage.PNG");
        secretQusPage = new Pattern("./Resources/images/LogInPage/secretQus.PNG");
        secretQusField = new Pattern("./Resources/images/LogInPage/secretQusField.PNG");
        vDriveCloudPage = new Pattern("./Resources/images/LogInPage/OnBoardingPage.PNG");
        invalidInfo = new Pattern("./Resources/images/LogInPage/invalidInfo.PNG");
        ErrorCode106Msg = new Pattern("./Resources/images/LogInPage/ErrorCode106.PNG");

        vDriveContinueButton = new Pattern("./Resources/images/LogInPage/vCloudCntButton.PNG");
        verificationSendButton = new Pattern("./Resources/images/LogInPage/verificationSendButton.PNG");
        verificationPage = new Pattern("./Resources/images/LogInPage/verificationPage.PNG");
        homePage = new Pattern("./Resources/images/LogInPage/homePage.PNG");
        secretQusContinueButton = new Pattern("./Resources/images/LogInPage/secretContinueButton.PNG");

       // window = getWindowDriver(loginWindow);


    }

    public void enterLoginData(String user, String pass) throws FindFailed {
        find(driver, loginWindow);
        find(driver,username);
        type(driver, username, user);
        type(driver, password, pass);
    }
    
    public void enterresetLoginData(String user, String pass) throws FindFailed {
        find(driver, resetLogInPage);
        find(driver,resetUserID);
        type(driver, resetUserID, user);
        type(driver, resetPassword, pass);
    }
    
    public LogInScreen clickLogInPageExist() {
        driver = Driver.getInstance();
        click(driver, LogInPageExist.exact());
        return this;
    }

    public LogInScreen clickLogIn() {
        driver = Driver.getInstance();
        click(driver, signinButton.exact());
        return this;
    }
    
    public LogInScreen clickresetLogIn() {
        driver = Driver.getInstance();
        click(driver, resetSignInButton.exact());
        return this;
    }

    public void provideSecretQus(String secans) {
        type(driver, secretQusField, secans);
    }

    public boolean isLoginWindowExist() {
        driver = getScreenDriver();
        return find(driver, loginWindow);
    }
    
    public boolean StartupCloudExist() {
        driver = getScreenDriver();
        return find(driver, StartupCloud.exact());
    }
    
    public boolean backUpContinueButtonExist() {
        driver = Driver.getInstance();
        return   find(driver, bkContinueButton.exact());
    }
    
    
    
    
    
    public boolean ContinuewithOutAddingButtonExist() {
        driver = Driver.getInstance();
        return   find(driver, ContinuewithOutAddingButton.exact());
    }
    
    public boolean StartupArrowIconExist() {
        driver = getScreenDriver();
        return find(driver, StartupArrowIcon.exact());
    }
    
    public boolean StartupContinueButtonExist() {
        driver = getScreenDriver();
        return find(driver, StartupContinueButton.exact());
    }
    
    public boolean isresetLoginWindowExist() {
        driver = getScreenDriver();
        return find(driver, resetLogInPage);
    }

    public boolean mainPageExist() {
        driver = getScreenDriver();
        return find(driver, homePage);
    }
    
    public boolean ErrorCode106MsgExist() {
        driver = getScreenDriver();
        return find(driver, ErrorCode106Msg);
    }
    
    public boolean invalidInfoPageExist() {
        driver = getScreenDriver();
        return find(driver, invalidInfo);
    }
    
    public boolean isvDriveOnboardingPageExist() {
        driver = getScreenDriver();
        return find(driver, vDriveCloudPage.exact());
    }
//----------------------Home Page
    public boolean isHomePageExist() {
        driver = getScreenDriver();
        return find(driver, homePage);
    }
    
    
    public boolean AnalyzingPageExist() {
        driver = getScreenDriver();
        return find(driver, analyzingPage.exact());
    }
    
    public boolean secterQusPageExist() {
        driver = getScreenDriver();
        return find(driver, secretQusPage.exact());
    }
    
    public boolean spinnerLogoExist() {
        driver = getScreenDriver();
        return find(driver, spinnerLogo);
    }
    
    public boolean VdriveContinueButtonExist() {
        driver = Driver.getInstance();
        return find(driver, vDriveContinueButton.exact());
    }

    public boolean reTryPageExist() {
        driver = getScreenDriver();
        return find(driver, retryPage);
    }
    
    public boolean AddDeviceButtonExist() {
    	 driver = getScreenDriver();
         return find(driver, addDeviceBT.exact());
 
    }

    public boolean verificationPageExist() {
        driver = getScreenDriver();
        return find(driver, verificationPage);
    }

    public boolean backupProgressPageExist() {
        driver = getScreenDriver();
        return find(driver, backupProgressPage);
    }

    public LogInScreen verificationSendButton() {
        driver = Driver.getInstance();
        click(driver, verificationSendButton);
        return this;
    }

    public LogInScreen clickbackUpContinueButton() {
        driver = Driver.getInstance();
        click(driver, bkContinueButton);
        return this;
    }

    public LogInScreen clickreTryButton() {
        driver = Driver.getInstance();
        click(driver, retryButton);
        return this;
    }


    public LogInScreen clickVdriveContinueButton() {
        driver = Driver.getInstance();
        click(driver, vDriveContinueButton.exact());
        return this;
    }

    public LogInScreen clickSecretContinueButton() {
        driver = Driver.getInstance();
        click(driver, secretQusContinueButton.exact());
        return this;
    }
    
    public LogInScreen ClickContinuewithOutAddingButton() {
        driver = Driver.getInstance();
        click(driver, ContinuewithOutAddingButton.exact());
        return this;
    }
    
    

    public LogInScreen clickAddDeviceButton() {
        driver = Driver.getInstance();
        click(driver, addDeviceBT);
        return this;
    }

    public boolean homePageExist() {
        driver = getScreenDriver();
        return find(driver, addDeviceBT.exact());
    }


 /*   public LogInScreen clickbackupPagecontinueButton() {
        driver = Driver.getInstance();
        click(driver, bkContinueButton);
        return this;
    } */

}
