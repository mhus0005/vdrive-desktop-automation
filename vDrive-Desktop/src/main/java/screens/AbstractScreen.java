package screens;

import org.sikuli.script.Pattern;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;

import common.CommonScreenActions;
import managers.Driver;

public class AbstractScreen extends CommonScreenActions {

    public static Screen getScreenDriver() {
        return Driver.getInstance();
    }

    public static Region getWindowDriver(Pattern window) {
        return Driver.getInstance(window);
    }

    public static Region getvCloudDriver(Pattern window) {

        return Driver.getInstance(window);
    }

    public double Sleep(long millis) throws InterruptedException {
        return Driver.sleep(millis);
    }
}