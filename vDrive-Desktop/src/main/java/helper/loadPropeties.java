package helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class loadPropeties {

   public Properties prop;

    public  loadPropeties() throws IOException {

        File src = new File("./configs/config.properties");
        FileInputStream fis= new FileInputStream(src);
        prop =new Properties();
        prop.load(fis);
    }

    public String getAppLocation(){
        return prop.getProperty("appLocation");
    }

}
