package cucumber;

import managers.Driver;

public class TestContext {
		
	public ScenarioContext scenarioContext;
	
	public Driver driver;
	
	public Driver getWebDriverManager() {
		return driver;
	}
	
	 public ScenarioContext getScenarioContext() {
		 return scenarioContext;
		 }


}
