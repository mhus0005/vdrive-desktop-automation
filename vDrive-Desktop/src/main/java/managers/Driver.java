package managers;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;


/**
 * @author Hemanth Kumar
 *
 * Jul 18, 2017
 */

public class Driver {
	private static Screen screenDriver;
	private static Region windowDriver;

	public static Screen getInstance() {
		if (screenDriver == null) {
			screenDriver = new Screen();
		}
		return screenDriver;
	}

	public static Region getInstance(Pattern window) {
		if (windowDriver == null) {
			try {
				windowDriver = getInstance().wait(window, 30);
			} catch (FindFailed findFailed) {
				findFailed.printStackTrace();
			}
		}
		return windowDriver;
	}

	public static double sleep(long millis) throws InterruptedException {
		Thread.sleep(millis);
		return millis;
	}
}



