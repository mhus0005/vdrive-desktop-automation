package common;



import managers.Driver;
import managers.Log;
import org.sikuli.basics.Settings;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;


/**
 * Common Screen Actions.
 */
public class CommonScreenActions {
    boolean flag = false;


    public void type(Screen sDriver, Pattern img, String text) {
        sDriver = Driver.getInstance();
        try {
            Log.info("Type the specified text into the Pattern object");
            sDriver.type(img, text);
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
            Log.info("Exception found");
            Log.info("couldn't find the specified image" + img.toString());
        }
    }

    public void click(Screen sDriver, Pattern img) {
        sDriver = Driver.getInstance();
        try {
            Log.info("Click the specified Pattern object");
            sDriver.click(img);
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
            Log.info("Exception found");
            Log.info("couldn't find the specified image" + img.toString());
            Assert.fail("couldn't find the specified image");
        }
    }

    public String getText(Region driver, Pattern image) {
        Settings.OcrTextRead = true;
        try {
            Log.info("Get the text of specified Pattern object");
            return driver.find(image).text();
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
            Log.info("Exception found");
            Log.info("couldn't find the specified image" + image.toString());
        }
        return null;
    }

    public boolean find(Screen sDriver, Pattern image) {
        //sDriver = Driver.getInstance();
        try {
            Log.info("Finding the specified Pattern object");
            sDriver.find(image);
            flag = true;
        } catch (FindFailed findFailed) {
            flag = false;
            Log.info("Exception found");
            Log.info("couldn't find the specified image" + image.toString());
        }
        return flag;
    }

    public boolean find(Region rDriver, Pattern image) {
        try {
            Log.info("Finding the specified Pattern object");
            rDriver.find(image);
            flag = true;
        } catch (FindFailed findFailed) {
            flag = false;
            Log.info("Exception found");
            Log.info("couldn't find the specified image" + image.toString());
        }
        return flag;
    }

    public double Sleep(long millis) throws InterruptedException {
        return Driver.sleep(millis);
    }

    public void hover(Screen sDriver, Pattern img) {
        sDriver = Driver.getInstance();
        try {
            Log.info("Click the specified Pattern object");
            sDriver.hover(img);
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
            Log.info("Exception found");
            Log.info("couldn't find the specified image" + img.toString());
            Assert.fail("couldn't find the specified image");
        }
    }

    public boolean wait(Screen sDriver, Pattern image) {
        sDriver = Driver.getInstance();
        try {
            Log.info("Finding the specified Pattern object");
            sDriver.wait(image,120);
            flag = true;
        } catch (FindFailed findFailed) {
            flag = false;
            Log.info("Exception found");
            Log.info("couldn't find the specified image" + image.toString());
        }
        return flag;
    }
}
