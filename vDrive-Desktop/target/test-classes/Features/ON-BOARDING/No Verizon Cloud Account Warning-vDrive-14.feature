@Scenario14
Feature: No Verizon Cloud Account Warning-vDrive-14




Scenario Outline: No Verizon Cloud Account Warning:Enter correct MDN but invalid password - 01.

		Given Open vDriver Desktop Application For invalid password Test
		When User enters correct "<Username>" and invalid "<Password>" and "<Secretans>" click on Sign in button
		Then Verify an error message 'The information you entered does not match the information we have on file'
		And Close vDriver Application vDrive14
		
		Examples:
      |Username|Password|
      |9082851135|sncrqa1244|
		
		
Scenario Outline: No Verizon Cloud Account Warning:Enter valid VZW login details and disable the network - 02.

		# PreCondtion - Disable the network.  
	
		Given Open vDriver Desktop Application disable the network Test 
		When User enters valid "<Username>" and "<Password>" and "<Secretans>" for Login and Verify an error message Unable to access the network    
		And Close vDriver Application vDrive14 
		
		Examples:
      |Username|Password|
      |9082851135|sncrqa123|
