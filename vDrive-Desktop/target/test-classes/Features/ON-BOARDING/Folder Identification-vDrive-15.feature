#@Regression
@Scenario15
Feature: Folder Identification-vDrive-15

Scenario Outline: Application automatically initiates directory analysis after the user selects 'Add Device' from the start-up screen.


	Given Open vDriver Desktop Application Directory analysis 
    When User enters "<Username>" and "<Password>" and "<Secretans>" for LogIn    
  	And User click on Add Device and Verify Setup Complete Screen
    Then Close vDriver Application vDrive15
    Examples:
      |Username|Password|Secretans|
      |9085668110|sncrqa123|pizza|